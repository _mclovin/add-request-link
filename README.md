# Add Request Link

In the case of trumpable torrents, add a request link ("RQ") to the right of "PL". When a clicks RQ, the ensuing request page should be loaded with most items (e.g. record label, catalogue number, and format) pre-populated.