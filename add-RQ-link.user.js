// ==UserScript==
// @name           RED: Add a request link ("RQ") to trumpable torrents
// @description    https://redacted.ch/forums.php?action=viewthread&threadid=35095
// @author         _
// @version        0.4
// @match          https://redacted.ch/torrents.php?id*
// @match          https://redacted.ch/requests.php*rq_torrentid*
// @run-at         document-end
// @namespace      _
// ==/UserScript==

(() => {
  'use strict';

  const nodes = document.querySelectorAll(".tl_notice");
  const groupid = new URL(window.location).searchParams.get("id");
  const torrentid = new URL(window.location).searchParams.get("rq_torrentid");

  const hitAPI = async (torrentid) => {
    try {
      const res = await fetch(`${location.origin}/ajax.php?action=torrent&id=${torrentid}`);
      return res.json();
    } catch (error) {
      console.log(error);
    }
  };

  const decodeJSON = payload => {
    var txt = document.createElement("textarea");
    txt.innerHTML = payload;
    return txt.value;
  };

  if (groupid) {
    nodes.forEach(node => {
      if (node.innerHTML !== "Trumpable") return;
      let target_span = node.parentNode.parentNode.firstElementChild;
      let torrentid = target_span.innerHTML.match(/id=([0-9]+)/)[1];
      target_span.insertAdjacentHTML("beforeend", `| <a class="tooltip button_pl" href="requests.php?action=new&groupid=${groupid}&rq_torrentid=${torrentid}">RQ</a>`);
    });
  } else if (torrentid) {
    hitAPI(torrentid).then(({response, status}) => {
      if (status !== "success") {
        console.log("API request failed; aborting.")
        return
      }
      const { torrent, group } = response;
      const formats = {
        MP3: "format_0",
        FLAC: "format_1",
        AAC: "format_2",
        AC3: "format_3",
        DTS: "format_4"
      };
      const encodings = {
        "192": "bitrate_0",
        "APS (VBR)": "bitrate_1",
        "V2 (VBR)": "bitrate_2",
        "V1 (VBR)": "bitrate_3",
        "256": "bitrate_4",
        "APX (VBR)": "bitrate_5",
        "V0 (VBR)": "bitrate_6",
        "320": "bitrate_7",
        "Lossless": "bitrate_8",
        "24bit Lossless": "bitrate_9"
      };
      const media = {
        CD: "media_0",
        DVD: "media_1",
        Vinyl: "media_2",
        Soundboard: "media_3",
        SACD: "media_4",
        DAT: "media_5",
        Cassette: "media_6",
        WEB: "media_7",
        "Blu-Ray": "media_8"
      };
      document.getElementById(formats[torrent.format]).checked = true;
      document.getElementById(encodings[torrent.encoding]).checked = true;
      document.getElementById(media[torrent.media]).checked = true;
      if (torrent.format === "FLAC" && torrent.media === "CD") {
        document.getElementById("logcue_tr").removeAttribute("class");
        document.getElementById("needlog").checked = torrent.hasLog;
        if (torrent.logScore > 0) {
          document.getElementById("minlogscore_span").removeAttribute("class");
          document.getElementById("minlogscore").value = 100;
        }
        document.getElementById("needcue").checked = torrent.hasCue;
        document.getElementById("needchecksum").checked = torrent.hasLog;
      }
      document.getElementsByName("recordlabel")[0].value = decodeJSON(torrent.remasterRecordLabel) || decodeJSON(group.recordLabel);
      document.getElementsByName("cataloguenumber")[0].value = decodeJSON(torrent.remasterCatalogueNumber) || decodeJSON(group.catalogueNumber);
      document.getElementsByName("year")[0].value = torrent.remasterYear || group.year;
      document.getElementById("description").value = "Requesting the non-trumpable version of this release."
    });
  } else {
    console.log("Oops, something went wrong.");
  }
})();